﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TestWebApp.Interfaces;
using TestWebApp.Models;
using TestWebApp.Utilities;
using TestWebApp.ViewModels;

namespace TestWebApp.Services
{
    public class UserService : BaseService<User>, IUserService
    {
        private bool isPersistent = false;

        private EFDbContext _db;
        private IUserMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserService(EFDbContext db, IUserMapper mapper, UserManager<User> userManager, SignInManager<User> signInManager) : base(db)
        {
            _db = db;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<IdentityResult> CreateAsync(UserRegisterViewModel newUser)
        {
            return await _userManager.CreateAsync(await _mapper.RegisterVMToModel(newUser), newUser.Password);
        }
        public new async Task<IdentityResult> CreateAsync(User newUser)
        {
            return await _userManager.CreateAsync(newUser);
        }


        public async Task DeleteAsync(string username)
        {
            var user = await GetUserAsync(username);
            await base.DeleteAsync(user);
        }

        public Task<bool> Exists(string username)
        {
            return Task.FromResult(_db.Users.Any(e => e.UserName == username));
        }

        public async Task<User?> GetUserAsync(string username)
        {
            var user = await _db.Users.Where(m => m.UserName == username).ToListAsync();
            return (user.Count == 1) ? user.First() : null;
        }

        public async Task<UserViewModel?> GetUserVMAsync(string username)
        {
            var user = await GetUserAsync(username);
            if (user == null)
            {
                return null;
            }
            return await _mapper.ModelToVM(user);
        }
        public async Task<UserRegisterViewModel?> GetUserRegisterVMAsync(string username)
        {
            var user = await GetUserAsync(username);
            if (user == null)
            {
                return null;
            }
            return await _mapper.ModelToRegisterVM(user);
        }

        public async Task<bool> UpdateAsync(UserRegisterViewModel updateUser)
        {
            var user = await GetUserAsync(updateUser.UserName);
            if (user == null)
            {
                return false;
            }

            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, updateUser.Password);

            
            user.UserName = updateUser.UserName;
            user.FullName = updateUser.FullName;
            user.Email = updateUser.Email;
            user.Birthdate = updateUser.Birthdate;
            user.PhoneNumber = updateUser.PhoneNumber;
            await base.UpdateAsync(user);
            return true;
        }

        public new async Task<List<UserViewModel>> ToListAsync()
        {
            var users = await base.ToListAsync();
            List<UserViewModel> result = new();
            foreach (var user in users)
            {
                result.Add(await _mapper.ModelToVM(user));
            }
            return result;
        }

        public async Task<SignInResult> SignIn(UserLoginViewModel loginViewModel)
        {
            return await _signInManager.PasswordSignInAsync(loginViewModel.Username, loginViewModel.Password, isPersistent, false);
        }

        public async Task SignIn(UserRegisterViewModel registerViewModel)
        {
            await _signInManager.SignInAsync(await _mapper.RegisterVMToModel(registerViewModel), isPersistent);
        }

        public async Task SignOut()
        {
            await _signInManager.SignOutAsync();
        }

        public async Task<bool> CheckPasswordAsync(User user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }
    }
}
