﻿using TestWebApp.Interfaces;
using TestWebApp.ViewModels;
using TestWebApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;
using TestWebApp.Mappers;
using Slugify;
using TestWebApp.Utilities;

namespace TestWebApp.Services
{
    public class PostService : BaseService<Post>, IPostService
    {
        private EFDbContext _dbContext;
        private IPostMapper _mapper;

        public PostService(EFDbContext db, IPostMapper mapper) : base(db)
        {
            _dbContext = db;
            _mapper = mapper;
        }

        public async Task CreateAsync(PostCreateViewModel post)
        {
            var result = await _mapper.CreateVMToModel(post);
            await base.CreateAsync(result);
        }

        public async Task<Post> GetPostAsync(string slug)
        {
            return await _dbContext.Posts.FirstAsync(p => p.Slug == slug);
        }

        public async Task<PostViewModel> GetPostVMAsync(string slug)
        {
            return await _mapper.ModelToVM(await GetPostAsync(slug));
        }
        public async Task<PostCreateViewModel> GetPostCreateVMAsync(string slug)
        {
            return await _mapper.ModelToCreateVM(await GetPostAsync(slug));
        }

        public new async Task<List<PostViewModel>> ToListAsync()
        {
            var posts = await base.ToListAsync();
            List<PostViewModel> postviews = new();
            foreach (var post in posts)
            {
                postviews.Add(await _mapper.ModelToVM(post));
            }
            return postviews;
        }

        public async Task UpdateAsync(PostCreateViewModel model)
        {
            SlugHelper slugHelper = new SlugHelper();

            var post = await GetPostAsync(model.Slug);
            post.UpdatedDate = DateTime.Now;
            post.Title = model.Title;
            post.Slug = slugHelper.GenerateSlug(model.Title);
            post.Content = model.Content;
            post.UserId = (await _dbContext.Users.FirstAsync(u => u.UserName == model.UserName)).Id;
            await base.UpdateAsync(post);
        }
        
        public async Task DeleteAsync(string slug)
        {
            var target = await GetPostAsync(slug);
            await base.DeleteAsync(target);
        }

        public bool Exists(string slug)
        {
            return _dbContext.Posts.Any(e => e.Slug == slug);
        }

    }
}
