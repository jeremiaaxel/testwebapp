﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using TestWebApp.Interfaces;
using TestWebApp.Models;

namespace TestWebApp.Services
{
    public class BaseService<T> : IBaseService<T> where T : class
    {

        private EFDbContext _db;

        public BaseService(EFDbContext dbContext)
        {
            _db = dbContext;
        }

        public async Task CreateAsync(T entity)
        {
            _db.Set<T>().Add(entity);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _db.Set<T>().Remove(entity);
            await _db.SaveChangesAsync();
        }

        public bool Exists(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<T> GetItemAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> ToListAsync()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _db.Set<T>().Update(entity);
            await _db.SaveChangesAsync();
        }
    }
}
