﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Slugify;
using System.Configuration;
using System.Linq;
using TestWebApp.Models;

namespace TestWebApp
{
    public static class DatabaseInitializer
    {
        public static void Initialize(EFDbContext context, bool debug = true)
        {
            int n = 10;

            if (debug)
            {
                context.Database.EnsureDeleted();
            }
            context.Database.EnsureCreated();

            if (debug)
            {
                if (context.Users.Any())
                {
                    Console.WriteLine(context.Users.Any());
                } else
                {
                    InitializeUser(context, n);
                }

                if (context.Posts.Any())
                {
                    Console.WriteLine(context.Users.Any());
                } else
                {
                    InitializePost(context, n);
                }

            } 
        }

        public static void InitializeUser(EFDbContext context, int n)
        {
            for (int i = 0; i < n; i++) 
            {
                context.Users.Add(new User
                {
                    FullName = Faker.Name.FullName(),
                    Email = Faker.Internet.Email(),
                    UserName = Faker.Name.First(),
                    PhoneNumber = Faker.Phone.Number(),
                });
            }
            context.SaveChanges();
        }

        public static void InitializePost(EFDbContext context, int n)
        {
            Random random = new Random();
            int userCount = context.Users.Count();

            for (int i = 0;i < n; i++)
            {
                string title = Faker.Country.Name() + " " + Faker.Company.Name();
                int randomInt = random.Next(1, userCount);
                SlugHelper helper = new SlugHelper();

                context.Posts.Add(new Post
                {
                    Title = title,
                    Slug = helper.GenerateSlug(title),
                    Content = Faker.Name.FullName() + " " + Faker.Country.Name(),
                    UserId = context.Users.First(a => a.Id.Contains(randomInt.ToString())).Id
                });
            }
            context.SaveChanges();
        }

    }
}
