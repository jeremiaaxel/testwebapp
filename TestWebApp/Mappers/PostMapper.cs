﻿using Microsoft.EntityFrameworkCore;
using TestWebApp.Interfaces;
using TestWebApp.Models;
using TestWebApp.Utilities;
using TestWebApp.ViewModels;
using Slugify;

namespace TestWebApp.Mappers
{
    public class PostMapper : IPostMapper
    {
        private EFDbContext _dbContext;
        public PostMapper(EFDbContext context)
        {
            _dbContext = context;
        }

        public async Task<PostViewModel> ModelToVM(Post post)
        {
            PostViewModel model = new PostViewModel{
                Id = post.Id,
                Title = post.Title,
                Slug = post.Slug,
                Content = post.Content,
                CreatedDate = post.CreatedDate,
            };

            // get models username from database
            model.UserName = (await _dbContext.Users.FirstAsync(u => u.Id == post.UserId)).UserName;
            return model;
        }

        public async Task<Post> VMToModel(PostViewModel post)
        {
            SlugHelper helper = new SlugHelper();
            Post model = new()
            {
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                CreatedDate = post.CreatedDate,
            };

            // get models UserId from database
            model.UserId = (await _dbContext.Users.FirstAsync(u => u.UserName == post.UserName)).Id;
            return model;
        }

        public async Task<Post> CreateVMToModel(PostCreateViewModel post)
        {
            SlugHelper helper = new SlugHelper();
            Post model = new()
            { 
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                UserId = (await _dbContext.Users.FirstAsync(u => u.UserName == post.UserName)).Id
            };
            return model;
        }
        public async Task<PostCreateViewModel> ModelToCreateVM(Post post)
        {
            SlugHelper helper = new SlugHelper();
            PostCreateViewModel model = new()
            {
                Title = post.Title,
                Slug = helper.GenerateSlug(post.Title),
                Content = post.Content,
                UserName = (await _dbContext.Users.FirstAsync(u => u.Id == post.UserId)).UserName
            };
            return model;
        }
    }
}
