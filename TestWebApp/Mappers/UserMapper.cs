﻿using TestWebApp.Interfaces;
using TestWebApp.Models;
using TestWebApp.ViewModels;

namespace TestWebApp.Mappers
{
    public class UserMapper : IUserMapper
    {
        public Task<UserViewModel> ModelToVM(User user)
        {
            UserViewModel userView = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            return Task.FromResult(userView);
        }

        /* 
         * <summary>DOES NOT MAP THE PASSWORD</summary>
         */
        public Task<User> RegisterVMToModel(UserRegisterViewModel user)
        {
            User newUser = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };
            return Task.FromResult(newUser);
        }

        public Task<User> VMToModel(UserViewModel user)
        {
            throw new NotImplementedException();
        }
        public Task<UserRegisterViewModel> ModelToRegisterVM(User user)
        {
            UserRegisterViewModel newUser = new()
            {
                UserName = user.UserName,
                Birthdate = user.Birthdate,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };
            return Task.FromResult(newUser);
        }

    }
}
