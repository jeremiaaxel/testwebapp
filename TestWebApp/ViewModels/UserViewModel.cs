﻿using System.ComponentModel.DataAnnotations;

namespace TestWebApp.ViewModels
{
    public class UserViewModel
    {
        public string? Email { get; set; }
        public string UserName { get; set; }
        public string? FullName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthdate { get; set; }
        public string? PhoneNumber { get; set; }
    }
}
