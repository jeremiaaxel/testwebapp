﻿namespace TestWebApp.ViewModels
{
    public class PostCreateViewModel : PostViewModel
    {
        public string Title { get; set; }
        public string Content { get; set; }

        public override string? Slug { get; set; }
        public override string? UserName { get; set; }

    }
}
