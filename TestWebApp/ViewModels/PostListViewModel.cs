﻿using TestWebApp.Interfaces;
using TestWebApp.Mappers;
using TestWebApp.Models;

namespace TestWebApp.ViewModels
{
    public class PostListViewModel
    {
        public List<PostViewModel> PostsList { get; set; }

        public PostListViewModel()
        {
            PostsList = new List<PostViewModel>();
        }

        public void Add(PostViewModel postVM)
        {
            PostsList.Add(postVM);
        }

        //public List<PostViewModel> ToList(EFDbContext db)
        //{
        //    IPostMapper mapper = new PostMapper(db);
        //    foreach (var post in db.Posts.ToList())
        //    {
        //        PostViewModel postView = mapper.Map(post);
        //        PostsList.Add(postView);
        //    }

        //    return PostsList;

        //}
    }
}
