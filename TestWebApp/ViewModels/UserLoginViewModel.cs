﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TestWebApp.ViewModels;

namespace TestWebApp.ViewModels
{
    public class UserLoginViewModel
    {
        [Column("Username")]
        [Required]
        public string Username { get; set; }

        [Column("Password")]
        [Required]
        public string Password { get; set; }
    }
}
