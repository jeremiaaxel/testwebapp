﻿using System.ComponentModel.DataAnnotations;

namespace TestWebApp.ViewModels
{
    public class UserRegisterViewModel : UserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password and confirmation not match.")]
        public string PasswordConfirmation { get; set; }
    }

}
