﻿using TestWebApp.Models;

namespace TestWebApp.ViewModels
{
    public class PostViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public virtual string Slug { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public string Content { get; set; }
        public virtual string UserName { get; set; }
    }
}
