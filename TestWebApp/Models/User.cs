﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestWebApp.Models
{
    public class User : IdentityUser
    {
        public string? FullName { get; set; }

        [Required]
        public override string UserName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyy}", ApplyFormatInEditMode = true)]
        public DateTime? Birthdate { get; set; }
        public ICollection<Post>? Posts { get; set; }
    }
}
