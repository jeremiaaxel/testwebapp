﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TestWebApp.Utilities;

namespace TestWebApp.Models
{
    public class Post : BaseModel
    {
        //public string Title
        //{
        //    get
        //    {
        //        return Title;
        //    }
        //    set
        //    {
        //        Title = value;
        //        Slug = Slugify.GenerateSlug(value);
        //    }
        //}

        public string Title { get; set; }
        public string Slug { get; set; }
        public string Content { get; set; }
        public string? UserId { get; set; }
    }
}
