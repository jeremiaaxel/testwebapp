﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;

namespace TestWebApp.Models
{
    public class EFDbContext: IdentityDbContext<User>
    {
        private readonly DbContextOptions _options;
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options)
        {
            _options = options;
        }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>().ToTable("Post");
        }
    }
}
