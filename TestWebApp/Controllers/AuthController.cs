﻿using Microsoft.AspNetCore.Mvc;
using TestWebApp.Interfaces;
using TestWebApp.ViewModels;

namespace TestWebApp.Controllers
{
    public class AuthController : Controller
    {
        private readonly IUserService _service;
        private readonly IUserMapper _mapper;

        public AuthController(IUserService userService, IUserMapper mapper)
        {
            _service = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(UserLoginViewModel loginResponse)
        {
            
            if (ModelState.IsValid)
            {

                var result = await _service.SignIn(loginResponse);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            } 

            // Authentication fails
            return View(loginResponse);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _service.SignOut();

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(UserRegisterViewModel registerResponse)
        {
            Dictionary<string, string> messages = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                var result = await _service.CreateAsync(registerResponse);

                if (result.Succeeded)
                {
                    await _service.SignIn(registerResponse);
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                foreach (KeyValuePair<string, string> entry in messages)
                {
                    ModelState.AddModelError(entry.Key, entry.Value);
                }

                ModelState.AddModelError(string.Empty, "Invalid Registration Attempt");
            }

            // Registration fails
            return View(registerResponse);
        }
    }
}
