﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestWebApp.Models;
using TestWebApp.ViewModels;
using TestWebApp.Utilities;
using Microsoft.AspNetCore.Authorization;
using TestWebApp.Interfaces;

namespace TestWebApp.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _service;
        private readonly IUserMapper _mapper;

        public UserController(IUserService userService, IUserMapper mapper)
        {
            _service = userService;
            _mapper = mapper;
        }

        // GET: User
        [Authorize]
        public async Task<IActionResult> Index()
        {
            return View(await _service.ToListAsync());
        }

        // GET: User/Details/5
        [Authorize]
        public async Task<IActionResult> Details(string? username)
        {
            if (username == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserVMAsync(username);

            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: User/Create
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create(UserRegisterViewModel user)
        {
            Dictionary<string, string> messages = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                var result = await _service.CreateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            return View(user);
        }

        // GET: User/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(string? username)
        {
            if (username == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserRegisterVMAsync(username);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(string username, UserRegisterViewModel user)
        {
            if (username != user.UserName)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var updateResult = await _service.UpdateAsync(user);
                    if (updateResult)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    ModelState.AddModelError(String.Empty, "Database error");
                }
                catch (DbUpdateConcurrencyException)
                {
                    var userExist = await _service.Exists(username);
                    if (!userExist)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View(user);
        }

        // GET: User/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(string? username)
        {
            if (username == null)
            {
                return NotFound();
            }

            var user = await _service.GetUserVMAsync(username);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: User/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string username)
        {
            await _service.DeleteAsync(username);
            return RedirectToAction(nameof(Index));
        }
    }
}
