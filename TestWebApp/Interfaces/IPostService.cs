﻿using TestWebApp.Models;
using TestWebApp.ViewModels;

namespace TestWebApp.Interfaces
{
    public interface IPostService : IBaseService<Post>
    {
        new Task<List<PostViewModel>> ToListAsync();
        Task<PostViewModel> GetPostVMAsync(string slug);
        Task<PostCreateViewModel> GetPostCreateVMAsync(string slug);
        Task<Post> GetPostAsync(string slug);
        Task CreateAsync(PostCreateViewModel post);
        Task UpdateAsync(PostCreateViewModel model);
        Task DeleteAsync(string slug);
        bool Exists(string slug);
    }
}
