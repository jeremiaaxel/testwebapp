﻿using Microsoft.AspNetCore.Identity;
using TestWebApp.Models;
using TestWebApp.ViewModels;

namespace TestWebApp.Interfaces
{
    public interface IUserService : IBaseService<User>
    {
        new Task<List<UserViewModel>> ToListAsync();
        Task<User?> GetUserAsync(string username);
        Task<UserViewModel> GetUserVMAsync(string username);
        Task<UserRegisterViewModel?> GetUserRegisterVMAsync(string username);
        Task<IdentityResult> CreateAsync(UserRegisterViewModel newUser);
        new Task<IdentityResult> CreateAsync(User newUser);
        Task<bool> UpdateAsync(UserRegisterViewModel updateUser);
        Task DeleteAsync(string username);

        Task<SignInResult> SignIn(UserLoginViewModel loginViewModel);
        Task SignIn(UserRegisterViewModel registerViewModel);

        Task SignOut();

        Task<bool> Exists(string username);
        Task<bool> CheckPasswordAsync(User user, string password);
    }
}
