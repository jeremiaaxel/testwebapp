﻿using TestWebApp.Models;
using TestWebApp.ViewModels;

namespace TestWebApp.Interfaces
{
    public interface IPostMapper
    {
        Task<PostViewModel> ModelToVM(Post post);
        Task<Post> VMToModel(PostViewModel post);
        Task<Post> CreateVMToModel(PostCreateViewModel post );
        Task<PostCreateViewModel> ModelToCreateVM(Post post);
    }
}
