﻿namespace TestWebApp.Interfaces
{
    public interface IBaseService<T>
    {
        Task CreateAsync(T entity);
        Task<T> GetItemAsync(T entity);
        Task<List<T>> ToListAsync();
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        bool Exists(T entity);
    }
}
