﻿using TestWebApp.Models;
using TestWebApp.ViewModels;

namespace TestWebApp.Interfaces
{
    public interface IUserMapper
    {
        Task<UserViewModel> ModelToVM(User user);
        Task<User> VMToModel(UserViewModel user);
        Task<User> RegisterVMToModel(UserRegisterViewModel user);
        Task<UserRegisterViewModel> ModelToRegisterVM(User user);
    }
}
