﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace TestWebApp.Utilities
{
    public class Routing
    {
        static List<CustomRoute> routes = new List<CustomRoute>();
        
        // add new routes here
        static Routing()
        {
            routes.Add(new CustomRoute("Login", "/login", "Auth", "Login"));
            routes.Add(new CustomRoute("Register", "/register", "Auth", "Register"));
            routes.Add(new CustomRoute("Home", "/", "Home", "Index"));
            routes.Add(new CustomRoute("Privacy", "/privacy", "Home", "Privacy"));

            //routes.Add(new CustomRoute("User Details", "/user/{username?}", "User", "Details"))
        }      
        
        public static void RegisterRoutes(IApplicationBuilder app)
        {
            var webApp = (WebApplication)app;

            foreach (CustomRoute route in routes)
            {
                webApp.MapControllerRoute(
                    name: route.Name,
                    pattern: route.Url,
                    defaults: new { controller = route.Controller, action = route.Action }
                    );
            }

            // default routing
            webApp.MapControllerRoute(
                name: "default",
                pattern: "{controller=home}/{action=index}/{id?}");
        }
        public static List<CustomRoute> Routes()
        {
            return routes;
        }
    }

    public class CustomRoute
    {
        private string name;
        private string url;
        private string controller;
        private string action;

        
        /// <param name="name">nama route </param>
        /// <param name="url">bentuk url</param>
        /// <param name="controller">nama controller</param>
        /// <param name="action">nama action di controller, if null then use</param>
        public CustomRoute(string name, string url, string controller, string? action)
        {
            this.name = name;
            this.url = url;
            this.controller = controller;
            if (action != null)
            {
                this.action = action;
            } else
            {
                this.action = name;
            }
        }

        public string Name { 
            get { return name; }
            set { name = value; }
        }
        public string Url {
            get { return url; }
            set { url = value; } 
        }
        public string Controller {
            get { return controller; }
            set { controller = value; } 
        }

        public string Action
        {
            get { return action; }
            set { action = value; }
        }
        
    }

}
