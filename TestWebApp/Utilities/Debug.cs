﻿namespace TestWebApp.Utilities
{
    public class Debug
    {
        public static void WL(Object obj)
        {
            Console.WriteLine(obj.ToString());
        }
        
        public static void W(Object obj)
        {
            Console.Write(obj.ToString());
        }
    }
}
